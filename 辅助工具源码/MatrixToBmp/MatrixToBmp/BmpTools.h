#pragma once

bool SaveBitmapToFile(CBitmap* bitmap, LPWSTR lpFileName);

typedef struct __st_bw_image
{
    int w;
    int h;
    unsigned char* img;

} st_bw_image;

bool SaveBWBmpFile(st_bw_image& input, LPWSTR lpFileName);

typedef struct __st_four_color_image
{
    int w;
    int h;
    char* img;

} st_four_color_image;

bool Save_4ColorImage_To_4BitBmpFile(st_four_color_image& input, LPWSTR lpFileName);
bool Save_4ColorImage_To_2BitBmpFile(st_four_color_image& input, LPWSTR lpFileName);