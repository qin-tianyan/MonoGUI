// StrTools.h

#ifndef __STRTOOLS_H__
#define __STRTOOLS_H__

CString GetFileName(CString& strFullPath);
CString GetFilePath(CString& strFullPath);
CString TrimAll(const CString& rs);
int Split(CStringArray& rAr, LPCTSTR pszSrc, LPCTSTR pszDelimiter);
CString AtoW(BYTE* asc_str, int len);

#endif //__STRTOOLS_H__