// CtrlMgt.h: interface for the CCtrlMgt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTRLMGT_H__BA1EC6B2_3ACA_4394_9EF4_EFB44C7A1948__INCLUDED_)
#define AFX_CTRLMGT_H__BA1EC6B2_3ACA_4394_9EF4_EFB44C7A1948__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCtrl;
class CEditorView;
class CAttributeDlg;

class CCtrlMgt  
{
public:
	enum {
		NO_MOVE = 0,
		MOVE_CENTER,
		MOVE_TOP,
		MOVE_BOTTOM,
		MOVE_LEFT,
		MOVE_RIGHT
	};

// 成员变量
public:
	CEditorView*		m_pView;
	CAttributeDlg*		m_pAttributeDlg;	// 属性列表的指针
	int m_nCount;							// 控件的数量
	BOOL m_bIsSetTabMode;					// 是否设置Tab序号模式
	CCtrl* m_pCtrls[ CTRL_MAX ];			// 构成一个控件指针数组

	int m_CX;	// 控件宽度
	int m_CY;	// 控件高度

	// 下面的变量及函数处理鼠标拖拽改变控件尺寸或位置的操作
	// （0：取消设定；1：移动；2：拖拽上边缘；3：拖拽下边缘；4拖拽左边缘；5：拖拽右边缘）
	int m_nDragMode;
	int m_nOrgX;	// 原点位置
	int m_nOrgY;	// 原点位置

// 成员函数
public:
	CCtrlMgt (CEditorView* pParent);
	virtual ~CCtrlMgt();

	void SetAttributeDlg (CAttributeDlg* pAttributeDlg);	// 设置属性列表
	void Show (CDC* pdc);									// 显示全部控件
	void Add (int nType);									// 添加一个控件
	BOOL DeleteSelected();									// 删除选中的控件
	void SetTopLeftPoint (int x, int y);					// 设置左上点
	void SetBottomRightPoint (int x, int y);				// 设置右下点
	BOOL Select (int x, int y);								// 选取
	BOOL GetCurSel (CCtrl** pCtrl, int* pnIndex);			// 得到处于选取状态的控件的指针
	BOOL InvertTabMode();									// 翻转Tab模式的状态
	void RenewAttribute();									// 刷新属性列表的显示
	BOOL ModifyDataBase (int nIndex, CString strValue);		// 修改属性（属性列表用）
	void SetDefaultSize (CCtrl* pCtrl);						// 根据控件尺寸设置默认值
	void AdjustTitle (BOOL bTitle);							// 给所有控件的Y值加TITLE_HEIGHT或者减TITLE_HEIGHT
	BOOL New();												// 恢复到新建状态
	BOOL Open (CString strFilePath);						// 从文件恢复模板
	BOOL Save (CString strFilePath);						// 将模板存入文件

	// 设置拖拽修改的模式（移动操作需要原点）
	void SetMouseDragMode (int nMode, int x, int y);
	// 处理鼠标拖拽操作
	void MouseDrag (int x, int y);
};

#endif // !defined(AFX_CTRLMGT_H__BA1EC6B2_3ACA_4394_9EF4_EFB44C7A1948__INCLUDED_)
