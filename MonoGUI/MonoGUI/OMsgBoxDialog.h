// OMsgBoxDialog.h: interface for the OMsgBoxDialog class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OMSGBOXDIALOG_H__)
#define __OMSGBOXDIALOG_H__

class OMsgBoxDialog : public ODialog
{
private:
	char m_sInformation[256];	// 信息字符串
	WORD m_wMsgBoxStyle;		// 对话框样式

public:
	OMsgBoxDialog ();
	virtual ~OMsgBoxDialog ();

	// 虚函数，绘制对话框
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

	// 创建MessageBox
	BOOL Create (OWindow* pParent, char* sTitle, char* sText, WORD wMsgBoxStyle, int ID);
};

#endif // !defined(__OMSGBOXDIALOG_H__)
