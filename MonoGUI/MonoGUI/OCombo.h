// OCombo.h: interface for the OCombo class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OCOMBO_H__)
#define __OCOMBO_H__


class OCombo : public OWindow
{
private:
	enum { self_type = WND_TYPE_COMBO };

	OEdit* m_pEdit;
	OList* m_pList;

public:
	OCombo();
	virtual ~OCombo();

public:
	// 创建组合框
	virtual BOOL Create
	(
		OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);

	// 绘制组合框
	virtual void Paint (LCD* pLCD);

	// 组合框消息处理
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

#if defined (MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc (OWindow* pWnd, int nMsg, int wParam, int lParam);
#endif // defined(MOUSE_SUPPORT)

	// 显示或者隐藏下拉列表框
	BOOL ShowDropDown (BOOL bShow);

	// 得到下拉列表框的显示状态
	BOOL GetDroppedState();

	// 设置下拉列表框的高度(以行数计)
	BOOL SetDroppedLinage (int nLinage);

	// 允许或者禁止对编辑框输入文字
	BOOL EnableEdit (BOOL bEnable);

	// 允许或者禁止下拉菜单弹出
	BOOL EnableDropDown (BOOL bEnable);

	// 以下函数用于操作编辑框
	// 限制文字的长度
	int LimitText (int nLength);

	// 清空编辑框
	BOOL Clean();

	// 取得编辑框的文字
	BOOL GetText (char* pText);

	// 设置编辑框的文字
	BOOL SetText (char* pText, int nLength);

	// 取得编辑框文字的长度
	int GetTextLength();

// 以下函数用于操作下拉列表框
	// 得到列表的条目数
	int GetCount();

	// 得到列表框当前选中项目的Index，如果没有选中的则返回-1
	int GetCurSel();

	// 设置列表框当前的选中项目
	int SetCurSel (int nIndex);

	// 获得某一列表项的内容
	BOOL GetString (int nIndex, char* pText);

	// 设置某一列表项的内容
	BOOL SetString (int nIndex, char* pText);

	// 获得某一列表项内容的长度
	int GetStringLength (int nIndex);

	// 向列表框中添加一个串(加在末尾)
	BOOL AddString (char* pText);

	// 删除列表框的一个列表项
	BOOL DeleteString (int nIndex);

	// 在列表框的指定位置插入一个串
	BOOL InsertString (int nIndex, char* pText);

	// 删除列表框的所有列表项
	BOOL RemoveAll();

	// 在列表框中查找一个串
	int FindString (char* pText);

	// 根据列表框的内容更新编辑框
	void SelectString (char* pText);

	// 将List当前的字符串拷贝到Edit中
	void SyncString();

private:
	// Edit得到焦点
	void EditSetFocus();

	// Edit失去焦点
	void EditKillFocus();

	// List得到焦点
	void ListSetFocus();

	// List失去焦点
	void ListKillFocus();

	// 判断是否可以向编辑框输入
	BOOL CanEdit();

	// 判断是否可以弹出列表框
	BOOL CanDropDown();
};

#endif // !defined(__OCOMBO_H__)
