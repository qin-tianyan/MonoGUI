// DlgShowEdit.cpp
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
//////////////////////////////////////////////////////////////////////
CDlgShowEdit::CDlgShowEdit()
{
}

CDlgShowEdit::~CDlgShowEdit()
{
}

// 消息处理过了，返回1，未处理返回0
int CDlgShowEdit::Proc (OWindow* pWnd, int nMsg, int wParam, int lParam)
{
	ODialog::Proc (pWnd, nMsg, wParam, lParam);

	if (pWnd = this)
	{
		if (nMsg == OM_NOTIFY_PARENT)
		{
			switch (wParam)
			{
			case 104:
				{
					OEdit* pEditStudent = (OEdit *)FindChildByID (102);
					OEdit* pEditSerial  = (OEdit *)FindChildByID (103);

					char student[LIST_TEXT_MAX_LENGTH];
					char serial[LIST_TEXT_MAX_LENGTH];
					memset (student, 0x0, sizeof(student));
					memset (serial, 0x0, sizeof(serial));

					pEditStudent->GetText(student);
					pEditSerial->GetText(serial);

					char info[LIST_TEXT_MAX_LENGTH * 2 + 100];
					sprintf (info, "姓名是：\n%s\n编码是：\n%s", student, serial);
					OMsgBox (this, "信息", info, OMB_INFORMATION | OMB_SOLID, 60);
				}
				break;

			case 105:
				{
					// 退出按钮
					O_MSG msg;
					msg.pWnd = this;
					msg.message = OM_CLOSE;
					msg.wParam = 0;
					msg.lParam = 0;
					m_pApp->PostMsg (&msg);
				}
				break;
			}
		}
	}

	return 1;
}

/* END */